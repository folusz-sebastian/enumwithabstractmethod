package com.companySF.enumWithAbstractMethod;

public enum HourlyPayGrade {
    APPRENTICE {
        public double rate() {
            return 1.0;
        }
    },
    LEUTENANT_JOURNEYMAN {
        public double rate() {
            return 1.2;
        }
    },
    JOURNEYMAN{
        public double rate() {
            return 1.5;
        }
    };
    public abstract double rate();

}
