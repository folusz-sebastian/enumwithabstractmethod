package com.companySF.enumWithAbstractMethod;

public class HourlyEmployee {
    private final HourlyPayGrade grade;

    public HourlyEmployee(HourlyPayGrade grade) {
        this.grade = grade;
    }

    public double calculatedRate() {
        return grade.rate();
    }
}
